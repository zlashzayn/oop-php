<?php
include "Frog.php";
include "Ape.php";
// release 0
class Animal {
    public $legs = 4;
    public $cold_blooded = "no";
    public $name;
    public function __construct($name)
    {
        $this->name = $name;
    }
}

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name;
echo "<br>legs : " . $sheep->legs;
echo "<br>cold blooded : " . $sheep->cold_blooded;
echo "<br>";
echo "<br>";

// release 1
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "Name : " . $kodok->name;
echo "<br>legs : " . $kodok->legs;
echo "<br>cold blooded : " . $kodok->cold_blooded;
echo "<br>jump : "; 
$kodok->jump();
echo "<br>";
echo "<br>";

echo "Name : " . $sungokong->name;
echo "<br>legs : " . $sungokong->legs;
echo "<br>cold blooded : " . $sungokong->cold_blooded;
echo "<br>Yell : "; 
$sungokong->yell();

?>